import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PrincipalPage} from '../pages/principal/principal';
import { AuthProvider } from '../providers/auth/auth';
import {RegisterPage} from '../pages/register/register';
import {PreferPage} from '../pages/prefer/prefer';
import {SearchPage} from '../pages/search/search';
import { CustomService } from '../providers/custom.service';
import {ToolbarComponent} from '../components/toolbar/toolbar';
import {ProfilePage} from '../pages/profile/profile';
import {SearchPipe} from '../pages/search/search.pipe';
import {UserDetailPage} from '../pages/user-detail/user-detail';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PrincipalPage,
    RegisterPage,
    PreferPage,
    SearchPage,
    ToolbarComponent,
    ProfilePage,
    SearchPipe,
    UserDetailPage,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PrincipalPage,
    RegisterPage,
    PreferPage,
    SearchPage,
    ToolbarComponent,
    ProfilePage,
    UserDetailPage,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    CustomService,
    Facebook
  ]
})
export class AppModule {}
