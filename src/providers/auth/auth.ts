import {  HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Observable, Subject } from 'rxjs';
import {_throw} from 'rxjs/observable/throw';
import { catchError, map, tap } from 'rxjs/operators';
import { RequestOptions } from '@angular/http';
import { User } from '../user.interface';


import { Injectable } from '@angular/core';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const api_key =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBlckFkbWluQXBpSyIsImp0aSI6IjVWZ0I3OGRUTW84SyJ9.a11218c04f0f374ac60ce4e3f329065d1b569d27d7b996c3021c59b7e41d9026';

@Injectable()
export class AuthProvider {

  public identity;
  public token;
  private httpOptions;
  public url = 'https://middleware-bathazar.herokuapp.com/beta';
 

  constructor(public http: HttpClient) {
    //console.log('Hello AuthProvider Provider');
    
   this.httpOptions = {
      headers: new HttpHeaders({
        'x-api-key': api_key,
        Authorization: this.getToken()
      })
    };
  }

  addNeeds(id: string, token, data) {
    const url = `${this.url}/users/${id}/needs`;
    const headers = new HttpHeaders({
      'x-api-key': api_key,
      Authorization: `Bearer ${token}`
    });
    return this.http.put<User>(url, data, { headers: headers }).pipe(
      catchError(err => {
        return _throw(err.error);
      })
    );
  }

  getUsers(token): Observable<User> {
    const headers = new HttpHeaders({
      'x-api-key': api_key,
      Authorization: `Bearer ${token}`
    });
    const url = `${this.url}/users`;
    return this.http
      .get<User>(url, {
        headers: headers
      })
      .pipe(
        catchError(err => {
          return _throw(err.error);
        })
      );
  }

  public getDistance(origin, destination) {
    const url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origin}&destinations=${destination}&key=AIzaSyDt6nnzbjEXxi3LfWRhEago1jSmL3HjV_g`;
    return this.http.get<any>(url).pipe(
      catchError(err => {
        return _throw(err.error);
      })
    );
  }

  addOffers(id: string, token, data) {
    const url = `${this.url}/users/${id}/offers`;
    const headers = new HttpHeaders({
      'x-api-key': api_key,
      Authorization: `Bearer ${token}`
    });
    return this.http.put<User>(url, data, { headers: headers }).pipe(
      catchError(err => {
        return _throw(err.error);
      })
    );
  }

  getToken() {
    const token = localStorage.getItem('token');
    token !== 'undefined' ? (this.token = token) : (this.token = null);
    return this.token;
  }

  signIn(user: User): Observable<User> {
    const url = `${this.url}/login`;
    const headers = new HttpHeaders({
      'x-api-key': api_key
    });
    return this.http
      .post<User>(url, user, {
        headers: headers
      })
      .pipe(
        catchError(err => {
          return _throw(err.error);
        })
      );
  }

  signFb(user: String): Observable<User> {
    const url = `${this.url}/login`;
    const headers = new HttpHeaders({
      'x-api-key': api_key
    });
    return this.http
      .post<User>(url, user, {
        headers: headers
      })
      .pipe(
        catchError(err => {
          return _throw(err.error);
        })
      );
  }

  signUp(user_register: User): Observable<User> {
    const url = `${this.url}/users`;
    const headers = new HttpHeaders({
      'x-api-key': api_key
    });
    return this.http
      .post<User>(url, user_register, {
        headers: headers
      })
      .pipe(
        catchError(err => {
          return _throw(err.error);
        })
      );
  }

   getIdentity() {
      const identity = JSON.parse(localStorage.getItem('identity'));
      identity !== 'undefined'
        ? (this.identity = identity)
        : (this.identity = null);
      return this.identity;
  }

  getUser(id: string, token): Observable<User> {
    const url = `${this.url}/users/${id}`;
    const headers = new HttpHeaders({
      'x-api-key': api_key,
      Authorization: `Bearer ${token}`
    });
    return this.http.get<User>(url, { headers: headers }).pipe(
      catchError(err => {
        return _throw(err.error);
      })
    );
  }

} 
