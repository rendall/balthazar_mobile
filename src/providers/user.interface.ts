export interface User extends DataRes {
  _id?: string;
  email: string;
  name?: string;
  password: string;
  rw_pass: string;
  address?: string;
  web?: string;
  photo?: string;
  user_type?: string;
  fb_token?: string;
  needs?: any;
  phone?: any;
  offers?: any;
}

interface DataRes {
  session?: any;
  message?: any;
  users?: any;
  user_info?: any;
}
