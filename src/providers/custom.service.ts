import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable()
export class CustomService {
  private selectionFormatState;
  private users;
  private home;

  constructor() {}
  setSearch(state: any) {
    this.selectionFormatState = state;
  }

  getSearch() {
    return this.selectionFormatState;
  }

  setUsers(state: any) {
    this.users = state;
  }

  getUsers() {
    return this.users;
  }

  homeState(state: any) {
    this.home = state;
  }
  getHome() {
    return this.home;
  }
}
