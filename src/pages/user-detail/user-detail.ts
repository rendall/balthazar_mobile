import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ToolbarComponent} from '../../components/toolbar/toolbar';
import { AuthProvider } from '../../providers/auth/auth';
import { User } from '../../providers/user.interface';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the UserDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-detail',
  templateUrl: 'user-detail.html',
})
export class UserDetailPage {
	public user: User;
  public identity;
  public token;
	para:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public authProvider: AuthProvider, public loading: LoadingController) {
  	this.para=navParams.data;
  	console.log(this.para);
  }

  ngOnInit() {
    this.identity = this.authProvider.getIdentity();
    this.token = this.authProvider.getToken();
    const id = this.navParams.data.id;
    this.getUser(id, this.token);
  }

  getUser(id, token) {
   var loader = this.loading.create({
	      content: "Please wait...", 
	      });
	      loader.present();
    this.authProvider.getUser(id, token).subscribe(
      response => {
       loader.dismiss();
        this.user = response.users[0];
      },
      err => {
        loader.dismiss();
        console.log(err.error);
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserDetailPage');
  }

}
