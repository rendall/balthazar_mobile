import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import {AuthProvider} from '../../providers/auth/auth';
import {User} from '../../providers/user.interface';
import {PrincipalPage} from '../principal/principal';
import {RegisterPage} from '../register/register';
import { LoadingController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[AuthProvider],

})
export class HomePage {
	 errorMsg: string;
  	 displayError: boolean;
	 user:User={
	 	email: 'dess1994@gmail.com',
	    name: '',
	    password: 'sifontes1994',
	    rw_pass: '',
	    session: '',
	    message: ''
	 }
   
   private signupForm: FormGroup;
  constructor(public navCtrl: NavController, private authProvider:AuthProvider, 
    public loading:LoadingController, private facebook:Facebook, private formBuilder:FormBuilder) {
      this.signupForm = formBuilder.group({
            email: new FormControl(null, [Validators.required, Validators.email]),
            password: new FormControl(null, Validators.required)
          });
  }
 
    

   irRegister(){
     this.navCtrl.push(RegisterPage);
   }
    
   loginFB() {
     var loader = this.loading.create({
      content: "Please wait...", 
      });
      loader.present();
      this.facebook.login(['email', 'user_likes','public_profile'])
        .then((res:FacebookLoginResponse)=>{
          console.log(res);
          const accessToken = res.authResponse.accessToken;
          
          this.authProvider.signFb(accessToken).subscribe(
            response => {
              loader.dismiss();
              this.navCtrl.push(PrincipalPage);
              console.log(response);
            },
            err => {
              loader.dismiss();
              alert(err);
            });
          if (!res.authResponse) {
            loader.dismiss();
            alert('The User declined something!');
          }
        })
        .catch(e=>{
          loader.dismiss();
          alert('Connected Error');
        })
    }
      

  onSubmit() {
    var loader = this.loading.create({
      content: "Please wait...",
      
    });
    loader.present();
    //this.loaderService.setState(true);
    this.authProvider.signIn(this.user).subscribe(
      response => {
        const user_id = response.session.user_id;
        const token = response.session.token;
        localStorage.setItem('identity', JSON.stringify(user_id));
        localStorage.setItem('token', token);
        //this.loaderService.setState(false);
        console.log(response);
        loader.dismiss();
        this.navCtrl.push(PrincipalPage);
      },
      error => {
        loader.dismiss();
        //this.loaderService.setState(false);
        this.errorMsg = error.message;
        console.log(this.errorMsg);
      }
    );
  }

}
