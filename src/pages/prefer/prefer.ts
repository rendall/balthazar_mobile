import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ViewChild, OnInit, Renderer, Input } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
/**
 * Generated class for the PreferPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-prefer',
  templateUrl: 'prefer.html',
})
export class PreferPage {
  identity;
  token;
  needs: any = [];
  offers: any = [];
  need: any;
  offer: any;
  step = 0;
  user;
  hash = true;
  value;
  warningNeed = false;
  warningOffer = false;
  blockNeed = false;
  blockOffer = false;
  itemsNeed: any = [];
  itemsOffers: any = [];



	accordionExapanded = false;
	accordionExapanded2 = false;
  @ViewChild("cc") cardContent: any;
  @ViewChild("cd") cardContent2: any;
  @Input('title') title: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  	public viewCtrl:ViewController, public renderer: Renderer, 
  	private authProvider: AuthProvider, public loading: LoadingController) {

  	this.identity = this.authProvider.getIdentity();
    this.token = this.authProvider.getToken();
    this.need = null;
  }

  ngOnInit() {
    console.log(this.cardContent.nativeElement);
    this.renderer.setElementStyle(this.cardContent.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
    this.renderer.setElementStyle(this.cardContent2.nativeElement, "webkitTransition", "max-height 500ms, padding 500ms");
  	
  	  this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 16px");
      this.renderer.setElementStyle(this.cardContent2.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent2.nativeElement, "padding", "0px 16px");
    this.getUserData();
  }

  close(): void {
    this.quitar();
  }

  addOffers() {
  		var loader = this.loading.create({
	      content: "Please wait...", 
	      });
	      loader.present();
	    this.blockOffer = false;
	    this.offers.forEach(element => {
	      this.itemsOffers.push(element.item.replace('#', ''));
	    });
	    const offers = {
	      offers: this.itemsOffers
	    };
	    this.authProvider.addOffers(this.identity, this.token, offers).subscribe(
	      response => {
	        loader.dismiss();
	        alert(response.message);
	        this.blockOffer = true;
	        this.close();
	      },
	      error => {
	        loader.dismiss();
	        alert(error.message);
	        this.blockOffer = true;
	      }
	    );
  }

   checkOffer(event) {
	    this.value = event.target.value;
	    if (this.value == '') {
	      this.warningOffer = false;
	    } else {
	      this.warningOffer = true;
	    }
	    this.offer = event.target.value;
	    if (this.offer[0] === '#') {
	      this.blockOffer = false;
	    } else {
	      this.offer = '';
	      this.blockOffer = false;
	    }
	    if (this.offer.indexOf('#') === -1) {
	      this.offer = `#${event.target.value}`;
	    } else {
	      if (this.offer.lastIndexOf('#')) {
	        this.blockOffer = false;
	        this.offer = '#';
	      }
	    }
  }

  saveOffer(event) {
	    if (this.offers > 0) {
	      this.blockOffer = true;
	    } else {
	      this.blockOffer = false;
	    }
	    this.offer = event.target.value.replace(/\s/g, '');
	    if (event.keyCode === 13) {
	      const offers = event.target.value.toLowerCase().match(/#([a-z0-9]+)/g);
	      if (offers == null) {
	        this.offer = '';
	      } else {
	        offers.forEach(element => {
	          if (this.offers.length >= 3) {
	            this.offer = '';
	          } else {
	            const values = [];
	            this.offers.push({
	              item: element.replace('#', ''),
	              color: this.getRandomColor()
	            });
	            this.blockOffer = true;
	            this.offers = this.removeDuplicates(this.offers, 'item');
	            this.offer = '';
	            this.warningOffer = false;
	          }
	        });
	      }
	    }
  }

  addNeeds() {
  		var loader = this.loading.create({
	      content: "Please wait...", 
	      });
	      loader.present();
	    this.blockNeed = false;
	    //this.loaderService.setState(true);
	    this.needs.forEach(element => {
	      this.itemsNeed.push(element.item.replace('#', ''));
	    });
	    const needs = {
	      needs: this.itemsNeed
	    };
	    this.authProvider.addNeeds(this.identity, this.token, needs).subscribe(
	      response => {
	        loader.dismiss();
	        this.step++;
	        alert(response.message);
	        this.blockNeed = true;
	        this.toggleAccordion2();

	      },
	      error => {
	        loader.dismiss();
	        alert(error.message);
	        this.blockNeed = true;
	      }
	    );
  }

  remove(array, x) {
    	array.splice(x, 1);
    	if (array.length === 0) {
      this.blockNeed = false;
      this.blockOffer = false;
    	}
  }

  getUserData() {
    this.authProvider.getUser(this.identity, this.token).subscribe(user => {
      this.user = user.users[0];
    });
  }

  checkNeed(event) {
	    this.value = event.target.value;
	    if (this.value == '') {
	      this.warningNeed = false;
	    } else {
	      this.warningNeed = true;
	    }
	    this.need = event.target.value;
	    if (this.need[0] === '#') {
	      this.blockNeed = false;
	    } else {
	      this.need = '';
	      this.blockNeed = false;
	    }
	    if (this.need.indexOf('#') === -1) {
	      this.need = `#${event.target.value}`;
	    } else {
	      if (this.need.lastIndexOf('#')) {
	        this.blockNeed = false;
	        this.need = '#';
	      }
	    }
	  }


  saveNeed(event) {
    	if (this.needs > 0) {
      	this.blockNeed = true;
    	} else {
      	this.blockNeed = false;
    	}
    	this.need = event.target.value.replace(/\s/g, '');
    	if (event.keyCode === 13) {
      const needs = event.target.value.toLowerCase().match(/#([a-z0-9]+)/g);
      if (needs == null) {
        this.need = '';
      } else {
        needs.forEach(element => {
          if (this.needs.length >= 3) {
            this.need = '';
          } else {
            const values = [];
            this.needs.push({
              item: element.replace('#', ''),
              color: this.getRandomColor()
            });
            this.blockNeed = true;
            this.needs = this.removeDuplicates(this.needs, 'item');
            this.need = '';
            this.warningNeed = false;
          	}
        	});
      	}
    	}
  	}

 private removeDuplicates(myArr, prop) {
	    return myArr.filter((obj, pos, arr) => {
	      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
	    });
  }

  	getRandomColor() {
	    const myArray = ['#e1f7d5', '	#ffbdbd', '#c9c9ff', '#f2f2f2', '#f1cbff'];
	    const color = myArray[Math.floor(Math.random() * myArray.length)];
	    return color;
  }

  toggleAccordion() {
  	  this.accordionExapanded2=false;
  	  this.renderer.setElementStyle(this.cardContent2.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent2.nativeElement, "padding", "0px 16px");

    	if (this.accordionExapanded) {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 16px");

    	} else {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "500px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "13px 16px");

    	}

    	this.accordionExapanded = !this.accordionExapanded;

  }

  toggleAccordion2() {
  		this.accordionExapanded=false;
  	  this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 16px");

    	if (this.accordionExapanded2) {
      		this.renderer.setElementStyle(this.cardContent2.nativeElement, "max-height", "0px");
      		this.renderer.setElementStyle(this.cardContent2.nativeElement, "padding", "0px 16px");

    	} else {
      	this.renderer.setElementStyle(this.cardContent2.nativeElement, "max-height", "500px");
      	this.renderer.setElementStyle(this.cardContent2.nativeElement, "padding", "13px 16px");

    	}

    	this.accordionExapanded2 = !this.accordionExapanded2;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferPage');
  }
  quitar(){
  	this.viewCtrl.dismiss();
  }
}
