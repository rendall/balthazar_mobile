import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {ModalController} from 'ionic-angular';
import { PreferPage} from '../prefer/prefer';
import {AuthProvider} from '../../providers/auth/auth';
import {CustomService} from '../../providers/custom.service';
import {LoadingController} from 'ionic-angular';
import {SearchPage} from '../search/search';

/**
 * Generated class for the PrincipalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-principal',
  templateUrl: 'principal.html',
})
export class PrincipalPage {

  user;
  hash = true;
  home = false;
  value: any;
  warning = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  	public modalCtrl:ModalController, public authProvider: AuthProvider, 
    public loading : LoadingController, public customService: CustomService) {
      const identity = this.authProvider.getIdentity();
      const token = this.authProvider.getToken();
      this.openTags(identity, token);
      this.home = this.customService.getHome();
  }

  checkHash(event) {
    this.value = event.target.value;
    if (this.value == '') {
      this.warning = false;
    } else {
      this.warning = true;
    }
    if (this.value[0] === '#') {
    } else {
      this.value = '';
    }
    if (this.value.indexOf('#') === -1) {
      this.value = `#${event.target.value}`;
    } else {
      if (this.value.lastIndexOf('#')) {
        this.value = '#';
      }
    }
  }

  ngOnInit() {
    if (this.home == undefined) {
      this.home = false;
    }
    if (!this.home) {
      this.customService.homeState(true);
    } else {
      this.customService.setSearch(undefined);
      this.navCtrl.push(SearchPage);
    }
  }

  onSearch(event: any) {
    if (event.which === 13) {
      this.value = event.target.value.replace(/\s/g, '');
      if (event.keyCode === 13) {
        const needs = event.target.value.toLowerCase().match(/#([a-z0-9]+)/g);
        const tags = [];
        if (needs == null) {
        } else {
          needs.forEach(element => {
            tags.push(element.replace('#', ''));
          });
          this.customService.setSearch(tags);
          this.navCtrl.push(SearchPage);
        }
      }
    }
  }

  openTags(id, token) {
    this.authProvider.getUser(id, token).subscribe(response => {
      this.user = response.users[0];
      if (!this.user.needs || !this.user.offers) {
        setTimeout(() => {
          this.open();
        }, 1);
      }
    });
  }

  open(): void {
    this.presentModal();
  }

  ionViewDidLoad() {
  	//this.presentModal();
    console.log('ionViewDidLoad PrincipalPage');
  }

  presentModal() {
    const modal = this.modalCtrl.create(PreferPage);
    modal.present();
  }

}
