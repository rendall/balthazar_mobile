import { Pipe, PipeTransform } from '@angular/core';
import { CustomService } from '../../providers/custom.service';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  tempo;
  constructor(private customService: CustomService) {}
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  transform(items: any[], args: any): any {
    let filteredItems: any[] = new Array();
    if (args.length > 0) {
      if (items) {
        items.forEach(item => {
          if (item.offers) {
            item.offers.forEach(tag => {
              args.forEach(value => {
                if (value.item) {
                  this.tempo = tag.includes(value.item);
                } else {
                  this.tempo = tag.includes(value);
                }
                if (this.tempo) {
                  filteredItems.push(item);
                  filteredItems = filteredItems.filter(this.onlyUnique);
                }
              });
            });
          }
        });
      }
      this.customService.setUsers(filteredItems.length);
      return filteredItems;
    } else {
      if (items) {
        this.customService.setUsers(items.length);
      }
      return items;
    }
  }
}
