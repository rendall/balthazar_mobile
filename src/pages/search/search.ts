import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ToolbarComponent} from '../../components/toolbar/toolbar';
import { AuthProvider } from '../../providers/auth/auth';
import { User } from '../../providers/user.interface';
import { catchError } from 'rxjs/operators';
import { LoadingController } from 'ionic-angular';
import { CustomService } from '../../providers/custom.service';
import {SearchPipe} from './search.pipe';
import {UserDetailPage} from '../user-detail/user-detail';
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
	public identity;
	  public token;
	  users;
	  loader = false;
	  search: any = [];
	  searchBox: any;
	  names: any;
	  dataOffers: any;
	  randomcolor: any;
	  items = [];
	  hidden;
	  usersValue: any = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  	public authProvider:AuthProvider, public loading:LoadingController, 
  	public customService: CustomService) {
	  	const data = this.customService.getSearch();
	    if (data == undefined) {
	      this.search = [];
	    } else {
	      this.search = this.customService.getSearch();
	      this.items.push({
	        item: this.customService.getSearch(),
	        color: this.getRandomColor()
	      });
	    }
  }

  goUserDetail(item){
  	this.navCtrl.push(UserDetailPage, item);
  	//console.log(item);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  checkHash(event) {
	    this.searchBox = event.target.value;
	    if (this.searchBox[0] === '#') {
	    } else {
	      this.searchBox = '';
	    }
	    if (this.searchBox.indexOf('#') === -1) {
	      this.searchBox = `#${event.target.value}`;
	    } else {
	      if (this.searchBox.lastIndexOf('#')) {
	        this.searchBox = '#';
	      }
	    }
	  }

  ngOnInit() {
	    this.identity = this.authProvider.getIdentity();
	    this.token = this.authProvider.getToken();
	    this.getUsers();
	    setTimeout(() => {
	      const userValue = this.customService.getUsers();
	      if (userValue > 4) {
	        this.hidden = 'block';
	      } else {
	        this.hidden = 'none';
	      }
	    }, 400);
  }

  onSearch(event: any) {
	    this.searchBox = event.target.value.replace(/\s/g, '');
	    if (event.which === 13) {
	      this.searchBox = event.target.value.replace(/\s/g, '');
	      if (event.keyCode === 13) {
	        const needs = event.target.value.toLowerCase().match(/#([a-z0-9]+)/g);
	        if (needs == null) {
	        } else {
	          needs.forEach(element => {
	            if (this.search.length >= 3) {
	              this.searchBox = '';
	            } else {
	              this.search.push(element.replace('#', ''));
	              this.randomcolor = this.getRandomColor();
	              this.search = this.search.filter(this.onlyUnique);
	              this.items.push({
	                item: element.replace('#', ''),
	                color: this.getRandomColor()
	              });
	              this.items = this.removeDuplicates(this.items, 'item');
	              this.searchBox = '';
	              setTimeout(() => {
	                const userValue = this.customService.getUsers();
	                if (userValue > 4) {
	                  this.hidden = 'block';
	                } else {
	                  this.hidden = 'none';
	                }
	              }, 50);
	            }
	          });
	        }
	      }
	    }
  }
  public getRandomColor() {
	    const myArray = ['#e1f7d5', '	#ffbdbd', '#c9c9ff', '#ffffff', '#f1cbff'];
	    const color = myArray[Math.floor(Math.random() * myArray.length)];
	    return color;
  }

  remove(array, x, element) {
	    array.splice(x, 1);
	    this.search = array.filter(function(e) {
	      return e !== element;
	    });
	    const user = this.customService.getUsers();
	    if (this.items.length === 0) {
	      this.hidden = 'block';
	    } else {
	      setTimeout(() => {
	        const userValue = this.customService.getUsers();
	        if (userValue > 4) {
	          this.hidden = 'block';
	        } else {
	          this.hidden = 'none';
	        }
	      }, 50);
	    }
  }

  getUsers() {
	  	var loader = this.loading.create({
	      content: "Please wait...", 
	      });
	      loader.present();
	    this.authProvider.getUsers(this.token).subscribe(
	      users => {
	      	loader.dismiss();
	        this.users = users.users;
	        this.authProvider
	          .getUser(this.identity, this.token)
	          .subscribe(response => {
	            console.log(response);
	            const origin = response.users[0].user_info.address;
	            this.users.forEach(element => {
	              if (
	                element.user_info.address &&
	                element.user_info.address != 'S/N'
	              ) {
	                console.log(origin);
	                const destination = element.user_info.address;
	                console.log(element.user_info.address);
	                this.authProvider
	                  .getDistance(origin, destination)
	                  .subscribe(distance => {
	                    if (
	                      distance.rows[0].elements[0].status !== 'ZERO_RESULTS'
	                    ) {
	                      element.user_info['distance'] =
	                        distance.rows[0].elements[0].distance.text;
	                    }
	                  });
	              }
	            });
	          });

	        console.log(this.users);
	        this.usersValue = this.users.length;
	      },
	      err => {
	        loader.dismiss();
	        console.log(err.error);
	      }
	    );
	    return this.users;
  }
  onlyUnique(value, index, self) {
    	return self.indexOf(value) === index;
  }
  private removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

}
