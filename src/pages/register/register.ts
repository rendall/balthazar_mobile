import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import {AuthProvider} from '../../providers/auth/auth';
import {User} from '../../providers/user.interface';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
	errorMsg: string;
	  displayError: boolean;
	  user: User = {
	    email: '',
	    name: '',
	    password: '',
	    rw_pass: '',
	    session: '',
	    message: '',
	    phone: '',
	    address: ''
	  };
	private signupForm: FormGroup;

  constructor(public loading:LoadingController, public navCtrl: NavController, 
  	public navParams: NavParams, private formBuilder:FormBuilder, public authProvider: AuthProvider) {
  	 this.signupForm = formBuilder.group({
  	 		name: new FormControl(null, [Validators.required]),
            email: new FormControl(null, [Validators.required, Validators.email]),
            rw_pass: new FormControl(null, [Validators.required]),
            password: new FormControl(null, Validators.required)
          });
  }

  irLogin(){
  	this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  onSubmit(newUser) {
  	 var loader = this.loading.create({
      content: "Please wait...",
      
    });
    loader.present();
    this.user.user_type = 'user';
    this.authProvider.signUp(this.user as User).subscribe(
      response => {
      	loader.dismiss();
        const message = response.message;
     	this.navCtrl.pop();
      },
      err => {
      	loader.dismiss();
        const errorMsg = err.message;
        alert(errorMsg);
      }
    );
  }

}
