import { Component } from '@angular/core';
import { User } from '../../providers/user.interface';
import { AuthProvider} from '../../providers/auth/auth';
import { Location } from '@angular/common';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrincipalPage} from '../../pages/principal/principal';
import {ProfilePage} from '../../pages/profile/profile';

/**
 * Generated class for the ToolbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'toolbar',
  templateUrl: 'toolbar.html'
})
export class ToolbarComponent {
  public identity;
  public token;
  user: User;



  constructor(private authProvider: AuthProvider, private location: Location, 
  	public navCtrl:NavController) {
   	this.identity = this.authProvider.getIdentity();
  }

  ngOnInit() {
    this.identity = this.authProvider.getIdentity();
    this.token = this.authProvider.getToken();
    this.getUser(this.identity, this.token);
  }
   getUser(id, token) {
    this.authProvider
      .getUser(id, token)
      .subscribe(
        response => (this.user = response),
        err => console.log(err.error)
      );
  }
  goBack() {
    this.navCtrl.pop();
  }
  goHome(){
  	this.navCtrl.push(PrincipalPage);
  }
  goProfile(){
  	this.navCtrl.push(ProfilePage);
  }

}
